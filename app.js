const express = require ('express');
const bodyParser = require('body-parser');
const assert = require('assert');
var objectId = require('mongodb').ObjectID;

const app = express();
const PORT = 5050;

const areas = require('./apis/areas')
const attractions = require('./apis/attractions')
const events = require('./apis/events')
const genres = require('./apis/genres')
const images = require('./apis/images')
const offers = require('./apis/offers')
const passwords = require('./apis/passwords')
const pricezones = require('./apis/pricezones')
const products = require('./apis/products')
const segments = require('./apis/segments')
const subgenres = require('./apis/subgenres')
const venues = require('./apis/venues')
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json()); 


app.use('/areas', areas)
app.use('/attractions', attractions)
app.use('/events', events)
app.use('/genres', genres)
app.use('/images', images)
app.use('/offers', offers)
app.use('/passwords', passwords)
app.use('/prices', prices)
app.use('/pricezones', pricezones)
app.use('/products', products)
app.use('/segments', products)
app.use('/segments', segments)
app.use('/subgenres', subgenres)
app.use('/venues', venues)



app.listen(PORT);
console.log("Listening on port " + PORT);