var express = require('express')
var router = express.Router()

const mongodb = require("mongodb");

const DB_NAME = require("ticket_master");
const ATTRACTIONS_COLLECTION_NAME = 'attractions';

const DB_URI = 'mongodb://localhost:27017';
const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, {useNewUrlParser: true, useUnifiedTopology: true, useNewUrlParser: true});

var objectId = require('mongodb').ObjectID;

router.get('/', function(req, res){

        client.connect(function(err, connection){
        if(err)
          return res.status(500).send({message:"Connection failed"});
    
            const db = connection.db(DB_NAME);
            db.collection(ATTRACTIONS_COLLECTION_NAME)
            .find({})
            .toArray(function(find_err, records){
    
                      if(find_err)
                        return res.status(500).send({message: "Something went wrong"});
      
                        res.send(records);
                        return res.status(200).send({message: "Record inserted successfully"});
            })
        })
    })
    router.post('/', function(req, res){
            if(!req.body.id || !req.body.type || !req.body.name || !req.body.images || !req.body.url || !req.body.classifications || !req.body.locale)
            return res.status(400).send({ message: "id, type, name, url, classifications, or locale is required."})
    
    
        if(!req.body || req.body.length === 0)
            return res.status(400).send({message: "Area data is required"})
    
        console.log(req.body); //print an object
        //data is in req.body
    
        client.connect(function(err, connection){
        if(err)
          return res.status(500).send({message:"Connection failed"});
            const db = connection.db(DB_NAME);
            db.collection(ATTRACTIONS_COLLECTION_NAME)
            .insertOne(req.body, function(insert_error, data){
                if(insert_error)
                    return res.status(500).send({message: "Something went wrong"});
                
                connection.close();
                return res.status(200).send({message: "Record inserted successfully"});
            })
        });
    });
    router.put('/:id', function (req, res) {

        if(!req.params.id || req.params.id === 0)
              return res.status(400).send({message: "Areas data is required"})
  
            client.connect(function(err, client){
            if(err)
              return res.status(500).send({message:"Connection failed"});
                const db = client.db(DB_NAME);
                db.collection(ATTRACTIONS_COLLECTION_NAME).updateOne({"_id" : objectId(req.params.id)},                 
                {$set: req.body},function(err, result) {
                  if(err)
                  return res.status(500).send({message: "Something went wrong"});
        
                    assert.equal(null, err);
                    return res.status(200).send({message: "Updated Successfully"});
        
            });
          });
        })
        router.delete('/:id', function(req, res){
                var id = req.params.id
            if(!req.params.id || req.params.id === 0)
                    return res.status(400).send({message: "Areas data is required"})
                client.connect(function(err, connection){
              if(err)
                return res.status(500).send({message:"Connection failed"});
                    const db = connection.db(DB_NAME);
                    assert.equal(null, err);
                        db.collection(ATTRACTIONS_COLLECTION_NAME).deleteOne({"_id": objectId(id)}, function(del_err, result){
                          if(del_err)
                            return res.status(500).send({message: "Something went wrong"});
                            assert.equal(null, err);
                            return res.status(200).send({message: "deleted Successfully"});
                })
              })
            })


    
    module.exports = router