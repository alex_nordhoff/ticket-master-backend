var express = require('express')
var router = express.Router()

const mongodb = require("mongodb");

const DB_NAME = require("ticket_master");
const SUBGENRES_COLLECTION_NAME = 'sugbgenres';

const DB_URI = 'mongodb://localhost:27017';
const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, {useNewUrlParser: true, useUnifiedTopology: true, useNewUrlParser: true});


//GET request
router.get('/', function(req, res){
    client.connect(function(err, connection){
        if(err){
            return res.status(500).send({message: "Something went wrong"});
        }
    
    const db = connection.db(DB_NAME); // Connection to the DB
    db.collection(SUBGENRES_COLLECTION_NAME)
    .find({})
    .toArray(function(find_err, records){
        if(find_err){
            return res.status(500).send({message: "Something went wrong"});
        }
    
        return res.status(200).send(records);
        })
    })
})

//POST request
router.post('/', function(req, res){
    if(!req.body.name)
            return res.status(400).send({ message: "name is required."})
    
    
        if(!req.body || req.body.length === 0)
            return res.status(400).send({message: "Seb Genre data is required"})
    
        console.log(req.body); //print an object
        //data is in req.body
    
        client.connect(function(err, connection){
            const db = connection.db(DB_NAME);
            db.collection(SUBGENRES_COLLECTION_NAME)
            .insertOne(req.body, function(insert_error, data){
                if(insert_error)
                    return res.status(500).send({message: "Something went wrong"});
                
                connection.close();
                return res.status(200).send({message: "Record inserted successfully"});
            })
        });
})

//PUT request
router.put('/:id', function(req, res){
    if(!req.params.id || req.params.id.length === 0)
                return res.status(400).send({message: "Sub Genre ID is required."})
    client.connect(function(err, connection){
            if (err){
                return res.status(500).send({message: "Something went wrong!"})
            }
        const db = connection.db(DB_NAME);
        db.collection(SUBGENRES_COLLECTION_NAME).updateOne({"_id" : objectId(req.params.id)},                 
        {$set: req.body},function(err, result) {
            if (err){
                return res.status(500).send({message: "Something went wrong!"})
            }

            assert.equal(null, err);
            return res.status(200).send({message: 'Updated successfully'})

    });
  });
})

//DELETE request
router.delete('/:id', function(req, res){
    if(!req.params.id || req.params.id.length === 0)
            return res.status(400).send({message: "Sub Genre ID is required."})
        var id = req.params.id
    
        client.connect(function(err, connection){
            if(err){
                return res.status(500).send({message: "Something went wrong."})
            }
            const db = connection.db(DB_NAME);
            assert.equal(null, err);
                db.collection(SUBGENRES_COLLECTION_NAME).deleteOne({"_id": objectId(id)}, function(del_err, result){
                    if (del_err){
                        return res.status(500).send({message: "Something went wrong."})
                    }
                    assert.equal(null, err);
                    return res.status(200).send({message: 'Deleted successfully'})
                })
        })
})
module.exports = router